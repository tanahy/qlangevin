import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
#from scipy.signal import argrelextrema
from tqdm import tqdm

Lambda = 10.
kB = 1
h = 1

#This class works for a chain of N osc with 3 baths:Left, Environment, Right
'''
def splitIntegrate(integrand,lim1,lim2,limit=50,split=100):
    i1 = integrate.quad(integrand,lim1,-split,limit=limit)
    i2 = integrate.quad(integrand,-split,0,limit=limit)
    i3 = integrate.quad(integrand,0,split,limit=limit)
    i4 = integrate.quad(integrand,split,lim2,limit=limit)
    return i1[0]+i2[0]+i3[0]+i4[0],i1[1]+i2[1]+i3[1]+i4[1]

def splitIntegrate(integrand,lim1,lim2,limit=50,split=100):
    i1 = integrate.quad(integrand,lim1,-split,limit=limit)
    i2 = integrate.quad(integrand,-split,split,limit=limit)
    i3 = integrate.quad(integrand,split,lim2,limit=limit)
    return i1[0]+i2[0]+i3[0],i1[1]+i2[1]+i3[1]
'''
def splitIntegrate(integrand,lim1,lim2,limit=50,split=100):
    #if abs(lim1)!=np.inf and abs(lim2)!=np.inf:
        #return (integrate.romberg(integrand,lim1,lim2,divmax=limit),0) #Doesnt work
        #result = (integrate.fixed_quad(integrand,lim1,lim2)[0],0) #Needs array treatment in sumAlpha, too much of a pain
                                                                  #After adapting, gives nan anyway...
        #result = integrate.quadrature(integrand,lim1,lim2,maxiter=limit) #Same as fixed_quad
        #print(result)
        #return result
    #else:
        return integrate.quad(integrand,lim1,lim2,limit=limit)

class Chain3(object):
    def __init__(self,D,NBaths,T=None,m=None,k=None,gamma=None,params=None,limit=50,intLim=None):
        
        # NBaths = (N,NL,NR)
        self.setBaths(NBaths)
        
        self.D = D
        
        if k is None:
            self.k = np.ones(self.N)#[0,*np.ones(self.N),0]
            self.k[-1] = 0
        elif isinstance(k,int) or isinstance(k,float):
            self.k = np.ones(self.N)*k
            self.k[-1] = 0
        else:
            self.k = k      #[0,*k, 0]
            self.k[-1] = 0
        if T is None:
            self.T = np.arange(3)
        else:
            self.T = T
        if params is None:
            #self.params = []
            self.w0 = np.ones(self.N)
        else:
            self.w0 = np.array(params[0])
        if m is None:
            self.m = np.ones(self.N)
        elif isinstance(m,int) or isinstance(m,float):
            self.m = np.ones(self.N)*m
        else:
            self.m = np.array(m)
        if gamma is None:
            self.gamma = 1e-3*np.ones(len(self.Baths))
        elif isinstance(gamma,int) or isinstance(gamma,float):
            self.gamma = np.ones(len(self.Baths))*gamma
        else:
            self.gamma = np.array(gamma)

        self.limit = limit
        if intLim is None:
            self.intLim = np.inf
        else:
            self.intLim = intLim

        self.KaCalc()
        #self.sigmaCalc()

    def setBaths(self,NBaths):
        # NBaths = (N,NL,NR)
        self.N ,self.NL,self.NR = NBaths
        self.Baths = range(self.NL),range(self.NL,self.N-self.NR),range(self.N-self.NR,self.N)
        if self.N ==2:
            self.Baths = [0],[1],[]
            self.NR = 0
            self.NL = 1

    def KaCalc(self):
        self.Ka = []
        self.W2 = []
        for i in range(self.N): #self.gamma*Lambda
            for l, bath in enumerate(self.Baths):
                if i in bath:
                    j = l
            self.Ka.append(integrate.quad(lambda w,i,j: 2/(np.pi)*np.imag(self.D(w,gamma=self.gamma[j],Lambda=Lambda,i=i,N=(self.N,self.NL,self.NR)))/w, 0, np.inf, args=(i,j))[0])
            W2i = self.w0[i]**2 + self.Ka[i]/self.m[i] + (self.k[i]+self.k[i-1])/self.m[i]
            #W2i = self.w0[i]**2 + self.Ka[-1]/self.m[i]
            #if i < self.N-1:
            #    W2i+= self.k[i]/self.m[i]
            #if i > 0:
            #    W2i += self.k[i-1]/self.m[i]
            #print(W2j,W2i)
            self.W2.append(W2i)

    def sigmaCalc(self):
        mask = np.triu(np.ones((2*self.N,2*self.N),dtype=bool))
        self.sigma = np.zeros((2*self.N,2*self.N))
        self.serr = np.zeros((2*self.N,2*self.N))
        self.sf = [[[] for j in range(2*self.N)] for i in range(2*self.N)]

        for i in tqdm(range(self.N)):
            #print('i:',i)
            for j in range(i,self.N):
                self.sigma[2*i,2*j],    self.serr[2*i,2*j],     self.sf[2*i][2*j]     = self.sxx(i,j)
                self.sigma[2*i+1,2*j+1],self.serr[2*i+1,2*j+1], self.sf[2*i+1][2*j+1] = self.spp(i,j)
                self.sigma[2*i,2*j+1],  self.serr[2*i,2*j+1],   self.sf[2*i][2*j+1]   = self.sxp(i,j)
                self.sigma[2*i+1,2*j],  self.serr[2*i+1,2*j],   self.sf[2*i+1][2*j]   = self.spx(i,j)

                if i!=j:
                    self.sigma[2*j,2*i] = self.sigma[2*i,2*j]
                    self.sigma[2*j+1,2*i+1] = self.sigma[2*i+1,2*j+1]
                    self.sigma[2*j+1,2*i] = self.sigma[2*i,2*j+1]
                    self.sigma[2*j,2*i+1] = self.sigma[2*i+1,2*j]

    def dQCalc(self):
        currents = []
        for bath in self.Baths:
            ci = 0.
            for i in bath:
                    #ci += self.W2[i]*self.spx(i,i)[0] this is 0!
                    for j in range(self.N):
                        if j!=i:
                            Sij = 0
                            if j==i+1:
                                Sij -= self.k[i]
                            if j==i-1:
                                Sij -= self.k[i-1]
                            if j in bath:
                                Sij += self.Ka[j]
                            if Sij != 0:
                                #print(i,j)
                                Sij *= (1/self.m[i])*self.spx(i,j)[0]
                                ci += Sij
            currents.append(ci)
        self.dQ = currents
        return self.dQ

    def c(self,i,j): #checkBath
        for bath in self.Baths:
            if i in bath:
                if j in bath:
                    return True
        return False

    def A(self,w):
        M = np.zeros((self.N,self.N),dtype=complex)
        for i in range(self.N):
            for l, bath in enumerate(self.Baths):
                if i in bath:
                    il = l
            M[i,i] += self.m[i]*(self.W2[i]-w**2)

            for j in range(self.N):
                if self.c(i,j):
                    M[i,j] += -self.D(w,gamma=self.gamma[il],Lambda=Lambda,i=j,N=(self.N,self.NL,self.NR))
                    if j != i:
                        M[i,j] += self.Ka[i]
            if i < self.N-1:
                M[i,i+1] += -self.k[i]
            if i > 0:
                M[i,i-1] += -self.k[i-1]

        return np.linalg.inv(M) #.pinv(M)

    def sumAlpha(self,w,i,j):
        Ai = 0+0.j
        A1 = self.A(w)
        A2 = self.A(-w)

        for k in range(self.N):
            for p, bath in enumerate(self.Baths):
                if k in bath:
                    for l in range(self.N):
                        if l in bath:
                            Ai += (A1[i,k]*A2[j,l])*(np.tanh(h*w/(2.*kB*self.T[p])))**(-1)*np.imag(self.D(w,gamma=self.gamma[p],Lambda=Lambda,i=l,N=(self.N,self.NL,self.NR)))

        return Ai

    def sumAlphaRe(self,w,i,j):
        Ai = 0#+0.j
        A = self.A(w)

        for k in range(self.N):
            for p, bath in enumerate(self.Baths):
                if k in bath:
                    for l in range(self.N):
                        if l in bath:
                            Ai += (A[i,k].real*A[j,l].real+A[i,k].imag*A[j,l].imag)*(np.tanh(h*w/(2.*kB*self.T[p])))**(-1)*np.imag(self.D(w,gamma=self.gamma[p],Lambda=Lambda,i=l,N=(self.N,self.NL,self.NR)))

        return Ai

    def sumAlphaIm(self,w,i,j):
        Ai = 0#+0.j
        A = self.A(w)

        for k in range(self.N):
            for p, bath in enumerate(self.Baths):
                if k in bath:
                    for l in range(self.N):
                        if l in bath:
                            Ai += (A[i,k].imag*A[j,l].real - A[i,k].real*A[j,l].imag)*(np.tanh(h*w/(2.*kB*self.T[p])))**(-1)*np.imag(self.D(w,gamma=self.gamma[p],Lambda=Lambda,i=l,N=(self.N,self.NL,self.NR)))

        return Ai



    def sxx(self,i,j):
        integrand = lambda w: h/(2*np.pi)*(self.sumAlpha(w,i,j)).real
        #integrand = lambda w: h/(2*np.pi)*(self.sumAlphaRe(w,i,j))
        result,err = splitIntegrate(integrand,-self.intLim,self.intLim,limit=self.limit)#integrate.quad
        return result,err,integrand


    def spp(self,i,j):
        integrand = lambda w: h/(2*np.pi)*self.m[i]*self.m[j]*w**2*(self.sumAlpha(w,i,j)).real
        #integrand = lambda w: h/(2*np.pi)*self.m[i]*self.m[j]*w**2*(self.sumAlphaRe(w,i,j))
        result,err = splitIntegrate(integrand,-self.intLim,self.intLim,limit=self.limit)#integrate.quad

        return result,err,integrand


    def sxp(self,i,j):
        integrand = lambda w: (-h/(2*np.pi)*self.m[j]*w*self.sumAlpha(w,i,j)).imag
        #integrand = lambda w: (-h/(2*np.pi)*self.m[j]*w*self.sumAlphaIm(w,i,j))
        if i == j:
            result,err = 0,0
        else:
            result,err = splitIntegrate(integrand,-self.intLim,self.intLim,limit=self.limit)#integrate.quad

        return result,err,integrand

    def spx(self,i,j):
        integrand = lambda w: (h/(2*np.pi)*self.m[i]*w*self.sumAlpha(w,i,j)).imag
        #integrand = lambda w: (h/(2*np.pi)*self.m[i]*w*self.sumAlphaIm(w,i,j))
        if i == j:
            #integrand = lambda w: w*0 
            result,err = 0,0
        else:
            #integrand = lambda w: (h/(2*np.pi)*self.m[i]*w*self.sumAlpha(w,i,j)).imag
            result,err = splitIntegrate(integrand,-self.intLim,self.intLim,limit=self.limit)#integrate.quad

        return result,err,integrand

    def tau(self):
        return [self.spp(i,i)[0]/(self.m[i]*kB)  for i in range(self.N)]


def D_DL(w,**kwargs):
    if 'gamma' in kwargs:
        gamma = kwargs['gamma']
    else:
        gamma = 1.e-3
    if 'm' in kwargs:
        m = kwargs['m']
    else:
        m = 1.
    if 'Lambda' in kwargs:
        Lambda = kwargs['Lambda']
    else:
        Lambda = 1000

    return m*gamma*Lambda**2/(w**2+Lambda**2)*(Lambda +1j*w)

def D_SF(w,**kwargs):
    if 'gamma' in kwargs:
        gamma = kwargs['gamma']
    else:
        gamma = 1.e-3
    if 'm' in kwargs:
        m = kwargs['m']
    else:
        m = 1.
    if 'Lambda' in kwargs:
        Lambda = kwargs['Lambda']
    else:
        Lambda = 1000

    return (2*Lambda +w*np.log(abs((w-Lambda)/(w+Lambda))))*m*gamma/np.pi + 1j*m*gamma*w*np.heaviside(Lambda-w,.5)*np.heaviside(Lambda+w,.5)

def D_SO(w,**kwargs):
    if 'gamma' in kwargs:
        gamma = kwargs['gamma']
    else:
        gamma = 1.e-3
    if 'm' in kwargs:
        m = kwargs['m']
    else:
        m = 1.
    if 'Lambda' in kwargs:
        Lambda = kwargs['Lambda']
    else:
        Lambda = 1000

    return m*gamma/(np.pi*Lambda)*(Lambda**2 +w**2*np.log(abs((w**2-Lambda**2)/(w**2)))) + 1j*m*gamma*w**2/Lambda*(np.heaviside(w,.5)*np.heaviside(Lambda-w,.5) - np.heaviside(-w,.5)*np.heaviside(Lambda+w,.5))

def D_Env1(w,**kwargs):
    if 'gamma' in kwargs:
        gamma = kwargs['gamma']
    else:
        gamma = 1.e-3
    if 'm' in kwargs:
        m = kwargs['m']
    else:
        m = 1.
    if 'Lambda' in kwargs:
        Lambda = kwargs['Lambda']
    else:
        Lambda = 1000
    if 'i' in kwargs:
        i = kwargs['i']
    else:
        i = 0
        print('Error with the index in Dalpha')
    if 'N' in kwargs:
        N,NL,NR = kwargs['N']
    else:
        N,NL,NR = (3,1,1)
        print('Error with the N count for Dalpha')

    if i >= NL and i< N-NR :
        return 0
    else:
        return (2*Lambda +w*np.log(abs((w-Lambda)/(w+Lambda))))*m*gamma/np.pi + 1j*m*gamma*w*np.heaviside(Lambda-w,.5)*np.heaviside(Lambda+w,.5)

def D_Env2(w,**kwargs):
    if 'gamma' in kwargs:
        gamma = kwargs['gamma']
    else:
        gamma = 1.e-3
    if 'm' in kwargs:
        m = kwargs['m']
    else:
        m = 1.
    if 'Lambda' in kwargs:
        Lambda = kwargs['Lambda']
    else:
        Lambda = 1000
    if 'i' in kwargs:
        i = kwargs['i']
    else:
        i = 0
        print('Error with the index in Dalpha')
    if 'N' in kwargs:
        N,NL,NR = kwargs['N']
    else:
        N,NL,NR = (3,1,1)
        print('Error with the N count for Dalpha')

    if i < NL:
        return (2*Lambda +w*np.log(abs((w-Lambda)/(w+Lambda))))*m*gamma/np.pi + 1j*m*gamma*w*np.heaviside(Lambda-w,.5)*np.heaviside(Lambda+w,.5)
    elif i >= N-NR:
        return m*gamma/(np.pi*Lambda)*(Lambda**2 +w**2*np.log(abs((w**2-Lambda**2)/(w**2)))) + 1j*m*gamma*w**2/Lambda*(np.heaviside(w,.5)*np.heaviside(Lambda-w,.5) - np.heaviside(-w,.5)*np.heaviside(Lambda+w,.5))
    else:
        return 0


def D_Env3(w,**kwargs):
    if 'gamma' in kwargs:
        gamma = kwargs['gamma']
    else:
        gamma = 1.e-3
    if 'm' in kwargs:
        m = kwargs['m']
    else:
        m = 1.
    if 'Lambda' in kwargs:
        Lambda = kwargs['Lambda']
    else:
        Lambda = 1000
    if 'i' in kwargs:
        i = kwargs['i']
    else:
        i = 0
        print('Error with the index in Dalpha')
    if 'N' in kwargs:
        N,NL,NR = kwargs['N']
    else:
        N,NL,NR = (3,1,1)
        print('Error with the N count for Dalpha')

    if i >= NL and i< N-NR :
        return 0
    else:
        return m*gamma/(np.pi*Lambda)*(Lambda**2 +w**2*np.log(abs((w**2-Lambda**2)/(w**2)))) + 1j*m*gamma*w**2/Lambda*(np.heaviside(w,.5)*np.heaviside(Lambda-w,.5) - np.heaviside(-w,.5)*np.heaviside(Lambda+w,.5))

def D_MIX(w,**kwargs):
    if 'gamma' in kwargs:
        gamma = kwargs['gamma']
    else:
        gamma = 1.e-3
    if 'm' in kwargs:
        m = kwargs['m']
    else:
        m = 1.
    if 'Lambda' in kwargs:
        Lambda = kwargs['Lambda']
    else:
        Lambda = 1000
    if 'i' in kwargs:
        i = kwargs['i']
    else:
        i = 0
        print('Error with the index in Dalpha')
    if 'N' in kwargs:
        N,NL,NR = kwargs['N']
    else:
        N,NL,NR = (3,1,1)
        print('Error with the N count for Dalpha')

    if i == 0:
        return m*gamma/(np.pi*Lambda)*(Lambda**2 +w**2*np.log(abs((w**2-Lambda**2)/(w**2)))) + 1j*m*gamma*w**2/Lambda*(np.heaviside(w,.5)*np.heaviside(Lambda-w,.5) - np.heaviside(-w,.5)*np.heaviside(Lambda+w,.5))
    else:
        return (2*Lambda +w*np.log(abs((w-Lambda)/(w+Lambda))))*m*gamma/np.pi + 1j*m*gamma*w*np.heaviside(Lambda-w,.5)*np.heaviside(Lambda+w,.5)
