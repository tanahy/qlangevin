from chain3 import *

plt.rcParams.update({'font.size': 14})
plt.rcParams.update({'figure.figsize':(8, 5)})

plt.rcParams.update({'legend.fontsize': 12})

sRange = np.logspace(1e-4,3.5,20)
sRange = np.logspace(-5,4,30)

T = [1e-3,1e-3]#,[1,1,0]#,[1000,1000,0]

dTRange = sRange*T[0]-T[1]

gamma = [1e+1,1e-0,1e-1,1e-2]
gamma = [1e-0,1e-1,1e-2]
Lambda = 10.

D = [D_SF,D_MIX,D_SO]
D = [D_SF,D_SO]
Dlabels = ['SF','MIX','SO']
Dlabels = ['OHM','SO']
markers = ['o','^','s','*']
markers = [',',',',',',',']

k = [1e+1,1e-0,1e-1,1e-2]

fig,axs = plt.subplots(len(gamma),len(D),sharex=True,sharey=True,tight_layout=True,gridspec_kw={'hspace': 0, 'wspace': 0}) #plt.figure()
fig2,(ax2,ax3) = plt.subplots(1,2,sharey=True,tight_layout=True,gridspec_kw={'wspace':0.1})
for i,Di in enumerate(D):
    #axs[i].set_title(f'$T_{{L,0}}={T[0]*sRange[0]:.2f},T_{{L,f}}={T[0]*sRange[-1]}$')
    print('Spectral Density:',Dlabels[i])
    c = np.ones((len(gamma),len(k)))
    for l in range(len(gamma)):
        for j in range(len(k)):
            a = []
            b = []
            print('gamma',gamma[l],'k',k[j])
            C1 = Chain3(Di, (2,1,1), T=T, m=1.,k=k[j], gamma=gamma[l],limit=100)
            for scale in sRange:
                #C1 = Chain3(Di, (2,1,1), T=T, m=1.,k=k[j], gamma=gamma[l],limit=100)
                Ti = T[:]
                Ti[0] = T[0]*scale
                #print(abs(Ti[0]-Ti[1]))
                C1.T = Ti
                dQ = C1.dQCalc()
                a.append(dQ[0])
                b.append(dQ[1])
                #print(Ti[0],dQ[0])
            #print(Ti)
            c[l,j] = dQ[0]

            
            #y = a*1/((a[1]-a[0])/(dTRange[1]-dTRange[0])*(dTRange-dTRange[0])+a[0])
            #c.append(a[-1])
            #axs[l,i].loglog(dTRange,y,marker=markers[j],label=f'$k={k[j]:.2f}$')
            axs[l,i].loglog(dTRange,a,marker=markers[j],label=f'$k={k[j]:.2f}$')
        
        #axs[l,i].set_xlim(1e0,dTRange[-1])
        
        #axs[l,i].loglog(dTRange,dTRange/dTRange,color='red',ls='dashed')
        axs[l,0].set_ylabel(f'$\dot{{Q}}(\gamma=10^{{{int(np.log10(gamma[l]))}}})$')
    for j in range(len(k)):
        if i==0: ax2.loglog(gamma,c[:,j],label=f'$k = {k[j]}$')
        if i==1: ax3.loglog(gamma,c[:,j],label=f'$k = {k[j]}$')
    axs[0,i].set_title(f'$\dot{{Q}}_L(D_{{{Dlabels[i]}}})$')
    axs[-1,i].set_xlabel('$\Delta T$')




print(dTRange)

ax2.legend()
ax2.set_xlabel('$\gamma$')
ax3.set_xlabel('$\gamma$')
ax2.set_ylabel('$\dot{Q}$')
ax2.set_title('OHM')
ax2.invert_xaxis()
ax3.set_title('SO')
ax3.invert_xaxis()
axs[0,0].legend()
#ax3.set_xticks([1e0,1e-1,1e-2])
#for ax in axs:
#    ax.label_outer()

fig.savefig(f'figures/dQdTmatrix{ "-".join(Dlabels)}.png')
fig2.savefig(f'figures/dQdkdgHT.png')
plt.show()







