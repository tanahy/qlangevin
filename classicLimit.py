from chain3 import *
plt.rcParams.update({'font.size': 16})
plt.rcParams.update({'figure.figsize':(8, 5)})

class CChain3(Chain3):
    def sumAlpha(self,w,i,j):
        Ai = 0+0.j
        A1 = self.A(w)
        A2 = self.A(-w)
        for k in range(self.N):
            for p, bath in enumerate(self.Baths):
                if k in bath:
                    for l in range(self.N):
                        if l in bath:
                            Ai += 1/h*(A1[i,k]*A2[j,l])*(w/(2.*kB*self.T[p]))**(-1)*np.imag(self.D(w,gamma=self.gamma[p],Lambda=Lambda))
        return Ai

class AChain3(Chain3):
    def sumAlpha(self,w,i,j):
        Ai = 0+0.j
        A1 = self.A(w)
        A2 = self.A(-w)
        for k in range(self.N):
            for p, bath in enumerate(self.Baths):
                if k in bath:
                    for l in range(self.N):
                        if l in bath:
                            Ai += (A1[i,k]*A2[j,l])*((2.*kB*self.T[p])/(w) +w/(6.*kB*self.T[p])-(w/(2.*kB*self.T[p]))**3/45.+2/945*(w/(2.*kB*self.T[p]))**5)*np.imag(self.D(w,gamma=self.gamma[p],Lambda=Lambda))
        return Ai




T = [1e-1,2e-1,3e-1]
T = [1e-1,1e-1]#,1e-2]
gamma = np.logspace(-1,1,3)#1e0

#i,j = 2,2

TRange = np.logspace(-1,2,10)

N = 2

stair = True

for stair in (0,1):
    for i in range(N):
        if stair:
            fig,axs = plt.subplots(2,1,sharex=True,tight_layout=True,gridspec_kw={'hspace':0.})
        else:
            fig,ax1 = plt.subplots()
            fig,ax2 = plt.subplots()
            axs = ax1,ax2
        j = i
        for l in range(len(gamma)):
    
            C1 = CChain3(D_DL,(N,1,1),gamma=gamma[l],limit=150)
            C2 = Chain3(D_DL,(N,1,1),gamma=gamma[l],limit=150)
            C3 = AChain3(D_DL,(N,1,1),gamma=gamma[l],limit=150)
    
            C1T = []
            C2T = []
            C3T = []
    
            for Ti in TRange:
                T[-1] = Ti
                if stair:
                    for k in range(len(T)):
                        T[k] = Ti*1/(1+k)
                C1.T = T
                C2.T = T
                C3.T = T
                C1.KaCalc()
                C2.KaCalc()
                C3.KaCalc()
    
                C1T.append(C1.sxx(i,j)[0])
                C2T.append(C2.sxx(i,j)[0])
                C3T.append(C3.sxx(i,j)[0])
    
            if l==0:
                axs[0].loglog(TRange,C1T,label='Classic')
                axs[0].loglog(TRange,C2T,label='Quantum')
                axs[0].legend()
    
            difference = abs(np.array(C2T)-np.array(C1T))
    
            axs[1].loglog(TRange,difference,marker='o',label=f'$\gamma={gamma[l]:.2f}$')
            axs[1].legend()
    
        #fig.add_subplot(313)
        #axs[2].bar(range(3),T/max(T))
    
        axs[0].set_ylabel(f'$\sigma_{{x_{i+1},x_{j+1}}}$')
        axs[1].set_ylabel(f'$|\sigma_{{x_{i+1},x_{j+1}}}^Q-\sigma_{{x_{i+1},x_{i+1}}}^C|$')
        axs[1].set_xlabel('$T$')
    
        if stair:
            fig.savefig(f'figures/classicLimit{i}stair.png')
        else:
            plt.tight_layout()
            fig.savefig(f'figures/classicLimit{i}step.png')

plt.show()
