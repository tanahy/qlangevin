import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
from matplotlib.ticker import FormatStrFormatter

plt.rcParams.update({'font.size': 16})
plt.rcParams.update({'figure.figsize':(8, 5)})
plt.rcParams.update({'legend.fontsize': 13})

gamma = 1e-1
Lambda = 10
m = 1
kB = 1
h = 1
T = 1
w0 = 1
#wR = np.sqrt(gamma*Lambda)

#w = np.linspace(-3*wR,3*wR,100)
w = np.linspace(-20,20,100)

L = 20


def ADL(w,gamma=gamma,T=T): #Drude-Lorentz
    wR = np.sqrt(gamma*Lambda)
    def D(w,gamma):
        return m*gamma*Lambda**2/(w**2+Lambda**2)*(Lambda+1.j*w)
    def alpha(w,gamma):
        return m*(w0**2 +wR**2-w**2)-D(w,gamma)
    A = alpha(w,gamma)**(-1)*alpha(-w,gamma)**(-1)*np.imag(D(w,gamma))*np.tanh(w*h/(2*kB*T))**(-1)*h/(2*np.pi)
    return np.real(A)

def ASF(w,gamma=gamma,T=T): #Step-Function
    #Lambda = 1/gamma
    wR = np.sqrt(2/np.pi*gamma*Lambda)
    def D(w,gamma):
        Dr = m*gamma/np.pi*(2*Lambda+w*np.log(abs((w-Lambda)/(w+Lambda))))
        Di = m*gamma*w*np.heaviside(Lambda-w,.5)*np.heaviside(Lambda+w,.5)
        return Dr + 1j*Di
    def alpha(w,gamma):
        return m*(w0**2 +wR**2-w**2)-D(w,gamma)
    A = alpha(w,gamma)**(-1)*alpha(-w,gamma)**(-1)*np.imag(D(w,gamma))*np.tanh(w*h/(2*kB*T))**(-1)*h/(2*np.pi)
    return np.real(A)


def ASO(w,gamma=gamma,T=T): #Step-Function
    #Lambda = 1/gamma
    wR = np.sqrt(2/np.pi*gamma*Lambda)
    def D(w,gamma):
        Dr = m*gamma/(np.pi*Lambda)*(Lambda**2+w**2*np.log(abs((Lambda**2-w**2)/w**2)))
        Di = m*gamma*w**2/Lambda*np.heaviside(Lambda-w,.5)*np.heaviside(w,.5)
        Di += -m*gamma*w**2/Lambda*np.heaviside(Lambda+w,.5)*np.heaviside(-w,.5)
        return Dr + 1j*Di
    def alpha(w,gamma):
        return m*(w0**2 +wR**2-w**2)-D(w,gamma)
    A = alpha(w,gamma)**(-1)*alpha(-w,gamma)**(-1)*np.imag(D(w,gamma))*np.tanh(w*h/(2*kB*T))**(-1)*h/(2*np.pi)
    return np.real(A)

def ASO(w,gamma=gamma,T=T): #Step-Function
    #Lambda = 1/gamma
    wR = np.sqrt(1/np.pi*gamma*Lambda)
    def D(w,gamma):
        Dr =  m*gamma/(np.pi*Lambda)*(Lambda**2+w**2*np.log(abs((Lambda**2-w**2)/w**2)))
        Di = m*gamma*w**2/Lambda*np.heaviside(Lambda-w,.5)*np.heaviside(w,.5)
        Di += -m*gamma*w**2/Lambda*np.heaviside(Lambda+w,.5)*np.heaviside(-w,.5)
        return Dr + 1j*Di
    def alpha(w,gamma):
        return m*(w0**2 +wR**2-w**2)-D(w,gamma)
    A = alpha(w,gamma)**(-1)*alpha(-w,gamma)**(-1)*np.imag(D(w,gamma))*np.tanh(w*h/(2*kB*T))**(-1)*h/(2*np.pi)
    return np.real(A)

sigmaSF = [["ASF(w,gamma,T)","0*w"],["0*w","ASF(w,gamma,T)*w**2*m**2"]]

sigmaDL = [["ADL(w,gamma,T)","0*w"],["0*w","ADL(w,gamma,T)*w**2*m**2"]]

sigmaSO = [["ASO(w,gamma,T)","0*w"],["0*w","ASO(w,gamma,T)*w**2*m**2"]]

sigmaHarmonic = [["h/(2*m*w0)*np.tanh(w0*h/(2*kB*T))**(-1)","0"],
            ["0","h*m*w0/2*np.tanh(w0*h/(2*kB*T))**(-1)"]]

def intij(array,i,j,gamma=gamma,T=T):
    element = array[i][j]
    integral = integrate.quad(lambda w,gamma,T: eval(element),-np.inf,np.inf, args = (gamma,T))
    return integral[0]

def evalij(array,i,j,gamma=gamma,T=T):
    element = array[i][j]
    return eval(element)
    return element

N = range(len(sigmaDL))


#E.1, E.2


gammaRange = np.linspace(1e-3,1,3)
TRange = np.logspace(-2,4,L)

fig4,axs = plt.subplots(3,1, tight_layout=True,sharex=True,gridspec_kw={'hspace':0})
fig5,ax5 = plt.subplots(3,1, tight_layout=True,sharex=True,gridspec_kw={'hspace':0})
#fig5,ax5 = plt.subplots(3,1)

TRange = np.logspace(-2,.1,L)

i = 0
wRange = (1,9,11)

labels = []
models = ["DL","SF",'SO',"Thermal"]
markers = ['o',',','^',',']
lstyles = ['None','dashed','dotted','solid']
for model in models:
    labels.append("$\sigma_{%s}$" % (model))
for j,ax in enumerate(axs):
    s = []
    c = []
    w0 = wRange[j]
    ax.set_ylabel(fr'$\sigma_{{xx}}$')
    ax5[j].set_ylabel(fr'$\tau$')
    ax.set_xlabel('$T$')
    ax5[j].set_xlabel('$T$')
    for Ti in TRange:
        s1 = intij(sigmaDL,i,i,gamma=gamma,T=Ti)
        s2 = intij(sigmaSF,i,i,gamma=gamma,T=Ti)
        s3 = intij(sigmaSO,i,i,gamma=gamma,T=Ti)
        s4 = evalij(sigmaHarmonic,i,i,gamma=gamma,T=Ti)

        s.append((s1,s2,s3,s4))
    #for Ti in TRange:
        s1 = intij(sigmaDL,1,1,gamma=gamma,T=Ti)
        s2 = intij(sigmaSF,1,1,gamma=gamma,T=Ti)
        s3 = intij(sigmaSO,1,1,gamma=gamma,T=Ti)
        s4 = evalij(sigmaHarmonic,1,1,gamma=gamma,T=Ti)

        c.append((s1,s2,s3,s4))

    s = np.array(s)
    if j == 1:
        s = s*100.
    c = np.array(c)
    for k in range(len(models)):
        ax.plot(TRange,s[:,k],ls=lstyles[k],marker=markers[k],label=labels[k])
        ax5[j].plot(TRange,c[:,k],ls=lstyles[k],marker=markers[k],label=labels[k])
    
    #ax.legend()
    #ax5[j].legend()
    #ax5[j].loglog(TRange,TRange)


    if j == 0:
        ax.set_ylim(4.8e-1,5.1e-1)
        ax5[0].set_ylim(4.9e-1,5.7e-1)
        ax.legend(loc='center right')
        ax5[j].legend(loc='center right')
    if j == 1:
        #ax.set_ylim(5.53e-2,5.56e-2)
        #ax.set_yticks([5.54,5.55,5.56])
        #ax5[1].set_ylim(4.49,4.52)
        ax5[1].set_yticks([4.5,4.51,4.52])
        ax.set_ylabel(r'$\sigma_{xx}\times 100$')
        #ax.set_ylabel(r'$\tau$')

        #ax.set_yticklabels(tick_labels.astype(int))
        #ax5[1].set_yticklabels(tick_labels.astype(int))
    if j == 2:
        ax.set_yscale('log')
        ax5[2].set_yscale('log')
        ax.set_xscale('log')
        ax5[2].set_xscale('log')

fig4.tight_layout()
fig5.tight_layout()
fig4.savefig('figures/SigmaXEvoT3-Lambda'+str(Lambda)+'.png')
fig5.savefig('figures/Temp-Lambda'+str(Lambda)+'.png')



plt.show()


