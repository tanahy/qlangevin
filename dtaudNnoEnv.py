from chain3 import *
plt.rcParams.update({'font.size': 16})
plt.rcParams.update({'figure.figsize':(8, 5)})

###
# Evolution of tau_i for all the chain with N_L for different T. The oscillators in the middle bath (environment) are not connected to any bath).
###

#gamma =1.e-1*np.ones(3)
Lambda = 10.
gamma = [1.e-2,0,1e-2]

k = 1.e-3

T = [2,1e-20,1]


TRange = [2,20] #np.logspace(0,1,2)#np.logspace(-2,1,10)
#TRange = [1,2,3]

Dmodels = [D_Env1,D_Env2,D_Env3]
Dmodels = [D_Env1,D_Env3]
Dlabels = ['OM','MIX','SO']
Dlabels = ['$D_{OHM}$','$D_{SO}$']

fig1,axes1 = plt.subplots(len(TRange),len(Dmodels),sharex='col',sharey='row',
                        gridspec_kw={'hspace': 0, 'wspace': 0})


N = 4#16
NR = 1
#m = 1.*np.ones(N)
#w0 = 1.*np.ones(N)

#fig1.suptitle(fr"$\ T_R={T[2]},\ N={N},\ N_{{R}}={NR} $")

#NLRange = range(1,N-NR)
NLRange = [1]#,int(N/4),int(N/3)]

dtau = [[[[] for i in range(len(TRange)) ] for l in range(len(NLRange))] for j in range(len(Dmodels))]
#for j in range(len(dtau)):
#    dtau[j] = np.empty((len(TRange),len(NRange)))

for j in range(len(Dmodels)):
    C2 = Chain3(Dmodels[j],(N,1,NR),T=T,m=1.,k=k,gamma=gamma,limit=80)
    for l in range(len(NLRange)):
        NL = NLRange[l]
        print(NL)
        C2.setBaths((N,NL,NL))

        for i in tqdm(range(len(TRange))):
            T[0] = TRange[i]
            C2.T = T
            C2.KaCalc()
            tau = C2.tau()
            #TdQ = sum(C2.dQCalc())
            #print('Total current:',TdQ)
            dtau[j][l][i] = tau

print('Printing in figure...')

for i in range(len(TRange)):
    for j in range(len(Dmodels)):
        for l in range(len(NLRange)):
            axes1[i][j].plot(range(1,N+1),dtau[j][l][i],marker='o',label=r"$N_L = %s$" % NLRange[l])
            axes1[i][j].axhline(TRange[i],c='red',ls='dotted')
            axes1[i][j].axhline(T[2],c='green',ls='dotted')
    #axes1[i][0].legend()
    #axes1[i][0].set_ylabel(r'$\dot{Q}_%s$' % i)
    #axes1[i][1].legend()

for i in range(len(Dmodels)):
    #axes1[0][i].legend()
    axes1[0][i].set_title(Dlabels[i])
    axes1[-1][i].set_xlabel('$i$')
for j in range(len(TRange)):
    axes1[j][0].set_ylabel(fr'$\tau (T_L={TRange[j]})$')
#axes1[2][0].set_xlabel('$T$')
#axes1[2][1].set_xlabel(r'$N_L$')
#axes2[2].set_xlabel(r'$N_L$')
#for ax in axes1.flat:
#    ax.label_outer()

fig1.savefig(f'figures/dQ1TNoEnv_{N}.png')

plt.show()
