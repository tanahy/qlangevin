from chain3 import *
plt.rcParams.update({'font.size': 16})
plt.rcParams.update({'figure.figsize':(8, 5)})

#####
# evolution of dQ under different baths with gamma_E, where the chain is broken into two pieces in the middle
####

# We want to see different baths and in principle TL > TE=TR

Lambda = 10.
gamma = 1e-1*np.ones(3)

N,NL,NR = 4,1,1#10,2,2
T = [2,1,1]
Tcases = [[2,1,1],[200,100,100]]
Tcases = [[2,1,1]]
k = 1e-2*np.ones(N)
k[int(N/2)-1] = 0.#1e-20#0.

#k = np.zeros(N)
#k[NL-1] = 1e-3
#k[N-NR-1] = 1e-3

gRange = np.logspace(-6,0,50)#,10)

thres = 5e-2
mask1 = gRange < thres

#print(mask1,mask2)

labels = ['L','E','R']

markers = ['o','s','^']

Dmodels = [D_SF]#,D_SO]

Dlabels = ['SF','SO']

#print('k:',k)

C1 = Chain3(D_SF,(N,NL,NR),T=T,k=k,gamma=gamma)

for i in range(len(Dmodels)):
    for j in range(len(Tcases)):
        T = Tcases[j]
        C1.T = T
        C1.D = Dmodels[i]
        dQ = []
        for gi in tqdm(gRange):
            gamma[1] = gi
            C1.gamma = gamma
            C1.KaCalc()
            dQi = C1.dQCalc()
            dQ.append(dQi)
        
        #print('N:',N)
        #print('gamma_E:', gRange)
        print('dQ:')
        print(dQ)
        
        dQ = np.array(dQ)
       
        fig,(ax1,ax2) = plt.subplots(1,2,sharey=True,tight_layout=True,gridspec_kw={'wspace':0.})

        #plt.title(f'$D={Dlabels[i]},T={T}$')
        for l in range(3):
            ax1.plot(gRange[mask1],dQ[:,l][mask1],marker='None',label=labels[l])
            ax2.plot(gRange,dQ[:,l],marker='None',label=labels[l])
        
        #plt.plot(gRange,np.sum(dQ,axis=1),label='$\sum \dot{Q}$')
        ax1.axhline(0,c='red',ls='dashed') 
        ax2.axhline(0,c='red',ls='dashed') 
        ax2.legend()
        
        ax1.set_xlabel(r'$\gamma_E$')
        ax2.set_xlabel(r'$\gamma_E$')
        ax1.set_ylabel('$\dot{Q}$')
        
        plt.savefig(f'figures/dQdE-{Dlabels[i]}-TR{T[2]}-N{N}.png')

plt.show()
