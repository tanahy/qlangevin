# QuantumLangevin

Code used for my master's thesis on energy transport in harmonic systems.

## Code Structure

The code implemented is object oriented to keep things readable and has been written in Python. The chain of oscillators have been implemented as a class and the scripts for each of the plots in the thesis report are separated into different files. File '''chain2.py''' is a deprecated implementation, kept for review purposes. 

```
from chain3 import *

N = 3
NL,NR = 1,1
w0 = 1.

C = Chainr3(D_DL,(N,NL,NR),k=1e-1,gamma=1e-1,params=(w0,)) 

# D_DL is an example of an ohmic spectral density function. You can implement you own very easily, see the definition on the main file chain3.py for more information
```


The `Chain3` class includes the case for the 2 oscillator and is faster than the old class, while being more general and is prepared to sustain 3 different baths. The extension to N baths is trivial but hasn't been implemented yet in order to simplify the syntax. The simple case of one oscillator and one bath has been implemented as functions in `sigmaEvo1.py` as that case is trivial. 

The rest of the files are uses of this class on specific scenarios.
