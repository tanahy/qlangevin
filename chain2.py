import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
#from scipy.signal import argrelextrema

gamma =[1.e-3,1.e-3]
Lambda = 1000#10.
m = [1,1]
kB = 1
k = 0.#1e-10#1.e-3
h = 1
T = [2e-1,3e-1]
T = [3e-1,2e-1]
w0 = [1.,1.]#[10,10]

class Chain2(object):
    def __init__(self,D,T=None,m=None,k=0,gamma=None,params=None):
        self.N = 2
        self.D = D
        self.k = k
        if T is None:
            self.T = [10.,20.]
        else:
            self.T = T
        if params is None:
            #self.params = []
            self.w0 = np.zeros(self.N)
        else:
            self.w0 = np.array(params[0])
        if m is None:
            self.m = np.array([])
        elif isinstance(m,int) or isinstance(m,float):
            self.m = np.ones(self.N)*m
        else:
            self.m = np.array(m)
        if gamma is None:
            self.gamma = [1e-3,1e-3]
        elif isinstance(gamma,int) or isinstance(gamma,float):
            self.gamma = np.ones(self.N)*gamma
        else:
            self.gamma = np.array(gamma)

        self.wR2 = [ integrate.quad(lambda w,i: 2/(self.m[i]*np.pi)*np.imag(self.D(i,w,m=self.m,gamma=self.gamma,Lambda=Lambda))/w, 0, np.inf, args=(i))[0] for i in range(self.N) ] #self.gamma*Lambda
        
        def sigmaCalc(self):
            self.sigma = np.zeros((2*self.N,2*self.N))
            self.serr = np.zeros((2*self.N,2*self.N))
            self.sf = [[[] for j in range(2*self.N)] for i in range(2*self.N)]
    
            for i in range(self.N):
                for j in range(self.N):
                    self.sigma[2*i,2*j],    self.serr[2*i,2*j],     self.sf[2*i][2*j]     = self.sxx(i,j)
                    self.sigma[2*i+1,2*j+1],self.serr[2*i+1,2*j+1], self.sf[2*i+1][2*j+1] = self.spp(i,j)
                    self.sigma[2*i,2*j+1],  self.serr[2*i,2*j+1],   self.sf[2*i][2*j+1]   = self.sxp(i,j)
                    self.sigma[2*i+1,2*j],  self.serr[2*i+1,2*j],   self.sf[2*i+1][2*j]   = self.spx(i,j)
    
    def alpha(self,w):
        A =  [self.m[j]*(self.w0[j]**2+self.wR2[j]-w**2)+self.k-self.D(j,w,m=self.m,gamma=self.gamma,Lambda=Lambda) for j in range(self.N)] #
        Delta = A[0]*A[1]-self.k**2
        alpha = np.array([[A[1]/Delta,self.k/Delta],[self.k/Delta,A[0]/Delta]])

        return alpha

    def sxx(self,i,j):
        c = lambda w,i,j,k: np.real(self.alpha(w)[i,k]*self.alpha(-w)[j,k])*np.tanh(h*w/(2.*kB*self.T[k]))**(-1)*np.imag(self.D(k,w,m=self.m,gamma=self.gamma,Lambda=Lambda))
        integrand = lambda w: h/(2*np.pi)*(c(w,i,j,0)+c(w,i,j,1))
        result,err = integrate.quad(integrand,-np.inf,np.inf,limit=100)

        return result,err,integrand


    def spp(self,i,j):
        c = lambda w,i,j,k: w**2*np.real(self.alpha(w)[i,k]*self.alpha(-w)[j,k])*np.tanh(h*w/(2.*kB*self.T[k]))**(-1)*np.imag(self.D(k,w,m=self.m,gamma=self.gamma,Lambda=Lambda))
        integrand = lambda w: h/(2*np.pi)*self.m[i]*self.m[j]*(c(w,i,j,0)+c(w,i,j,1))
        result,err = integrate.quad(integrand,-np.inf,np.inf,limit=100)

        return result,err,integrand


    def sxp(self,i,j):
        c = lambda w,i,j,k: 1j*w*self.alpha(w)[i,k]*self.alpha(-w)[j,k]*np.tanh(h*w/(2.*kB*self.T[k]))**(-1)*np.imag(self.D(k,w,m=self.m,gamma=self.gamma,Lambda=Lambda))
        integrand = lambda w: (h/(2*np.pi)*self.m[j]*(c(w,i,j,0)+c(w,i,j,1))).real
        result,err = integrate.quad(integrand,-np.inf,np.inf,limit=100)

        return result,err,integrand

    def spx(self,i,j):
        c = lambda w,i,j,k: -1j*w*self.alpha(w)[i,k]*self.alpha(-w)[j,k]*np.tanh(h*w/(2.*kB*self.T[k]))**(-1)*np.imag(self.D(k,w))
        integrand = lambda w: (h/(2*np.pi)*self.m[i]*(c(w,i,j,0)+c(w,i,j,1))).real
        result,err = integrate.quad(integrand,-np.inf,np.inf,limit=100)

        return result,err,integrand

    def dQCalc(self):
        #a,b = self.spx(0,0)[0],self.spx(1,1)[0]
        #print(a,b)
        a,b = 0,0
        dQ1 =  (self.w0[0] + self.wR2[0] + self.k/self.m[0])*a + (- 1/self.m[0]*self.k)*self.spx(0,1)[0]
        dQ2 =  (self.w0[1] + self.wR2[1] + self.k/self.m[1])*b + (- 1/self.m[1]*self.k)*self.spx(1,0)[0]
        return [dQ1,dQ2]

def D_DL(i,w,**kwargs):
    if 'gamma' in kwargs:
        gamma = kwargs['gamma']
    else:
        gamma = [1.e-3,1.e-3]
    if 'm' in kwargs:
        m = kwargs['m']
    else:
        m = [1,1]
    if 'Lambda' in kwargs:
        Lambda = kwargs['Lambda']
    else:
        Lambda = 1000

    return m[i]*gamma[i]*Lambda**2/(w**2+Lambda**2)*(Lambda +1j*w)

def D_SF(i,w,**kwargs):
    if 'gamma' in kwargs:
        gamma = kwargs['gamma']
    else:
        gamma = [1.e-3,1.e-3]
    if 'm' in kwargs:
        m = kwargs['m']
    else:
        m = [1,1]
    if 'Lambda' in kwargs:
        Lambda = kwargs['Lambda']
    else:
        Lambda = 1000

    return (2*Lambda +w*np.log(abs((w-Lambda)/(w+Lambda))))*m[i]*gamma[i]/np.pi + 1j*m[i]*gamma[i]*w*np.heaviside(Lambda-w,.5)*np.heaviside(Lambda+w,.5)

def D_SO(i,w,**kwargs):
    if 'gamma' in kwargs:
        gamma = kwargs['gamma']
    else:
        gamma = [1.e-3,1.e-3]
    if 'm' in kwargs:
        m = kwargs['m']
    else:
        m = [1,1]
    if 'Lambda' in kwargs:
        Lambda = kwargs['Lambda']
    else:
        Lambda = 1000

    return m[i]*gamma[i]/(np.pi*Lambda)*(Lambda**2 +w**2*np.log(abs((w**2-Lambda**2)/(w**2)))) + 1j*m[i]*gamma[i]*w**2/Lambda*(np.heaviside(w,.5)*np.heaviside(Lambda-w,.5) - np.heaviside(-w,.5)*np.heaviside(Lambda+w,.5))


def D_MIX(i,w,**kwargs):
    if 'gamma' in kwargs:
        gamma = kwargs['gamma']
    else:
        gamma = [1.e-3,1.e-3]
    if 'm' in kwargs:
        m = kwargs['m']
    else:
        m = [1,1]
    if 'Lambda' in kwargs:
        Lambda = kwargs['Lambda']
    else:
        Lambda = 1000

    if i == 0:
        return m[i]*gamma[i]/(np.pi*Lambda)*(Lambda**2 +w**2*np.log(abs((w**2-Lambda**2)/(w**2)))) + 1j*m[i]*gamma[i]*w**2/Lambda*(np.heaviside(w,.5)*np.heaviside(Lambda-w,.5) - np.heaviside(-w,.5)*np.heaviside(Lambda+w,.5))
    else:
        return (2*Lambda +w*np.log(abs((w-Lambda)/(w+Lambda))))*m[i]*gamma[i]/np.pi + 1j*m[i]*gamma[i]*w*np.heaviside(Lambda-w,.5)*np.heaviside(Lambda+w,.5)

def D_MIX2(i,w,**kwargs):
    if 'gamma' in kwargs:
        gamma = kwargs['gamma']
    else:
        gamma = [1.e-3,1.e-3]
    if 'm' in kwargs:
        m = kwargs['m']
    else:
        m = [1,1]
    if 'Lambda' in kwargs:
        Lambda = kwargs['Lambda']
    else:
        Lambda = 1000

    if i == 0:
        return m[i]*gamma[i]/(np.pi*Lambda)*(Lambda**2 +w**2*np.log(abs((w**2-Lambda**2)/(w**2)))) + 1j*m[i]*gamma[i]*w**2/Lambda*(np.heaviside(w,.5)*np.heaviside(Lambda-w,.5) - np.heaviside(-w,.5)*np.heaviside(Lambda+w,.5))
    else:
        return m[i]*gamma[i]*Lambda**2/(w**2+Lambda**2)*(Lambda +1j*w)


def fidelity2(s1,s2):
    J = np.zeros((4,4)) #Defined as J_kl = -i[R_k,R_l]
    '''
    Thus, J_kk = 0;
    J_0,1 = J_2,3 = h;
    J_1,0 = J_3,2 = - h;
    Others: J_k,l = 0
    '''
    J[0,1], J[2,3] = h,h
    J[1,0], J[3,2] = -h,-h
    a = np.linalg.det(s1+s2)
    b = 2**4*np.linalg.det(np.dot(np.dot(J,s1),np.dot(J,s2))-np.identity(4)/4)
    c = 2**4*np.linalg.det(s1+1j*J/2)*np.linalg.det(s2+1j*J/2)
    #print(a,b,c)
    return np.real(np.sqrt(b)+np.sqrt(c)-np.sqrt((np.sqrt(b)+np.sqrt(c))**2-a))**(-1)
