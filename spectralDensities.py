import numpy as np
import matplotlib.pyplot as plt

plt.rcParams.update({'font.size': 14})
plt.rcParams.update({'figure.figsize':(8, 5)})

def D_DL(w,gamma):
    return m*gamma*Lambda**2/(w**2+Lambda**2)*(Lambda+1.j*w)
def D_SF(w,gamma):
    Dr = m*gamma/np.pi*(2*Lambda+w*np.log(abs((w-Lambda)/(w+Lambda))))
    Di = m*gamma*w*np.heaviside(Lambda-w,.5)*np.heaviside(Lambda+w,.5)
    return Dr+1j*Di
def D_SO(w,gamma):
    Dr = m*gamma/(np.pi*Lambda)*(Lambda**2+w**2*np.log(abs((Lambda**2-w**2)/w**2)))
    Di = m*gamma*w**2/Lambda*np.heaviside(Lambda-w,.5)*np.heaviside(w,.5)
    Di += -m*gamma*w**2/Lambda*np.heaviside(Lambda+w,.5)*np.heaviside(-w,.5)
    return Dr + 1j*Di

def J_DL(w):
    return m*gamma*w*Lambda**2/(w**2+Lambda**2)
def J_SF(w):
    return m*gamma*w*np.heaviside(Lambda-w,0)*np.heaviside(w,0)
def J_SO(w):
    Di = m*gamma*w**2/Lambda*np.heaviside(Lambda-w,0)*np.heaviside(w,0)
    return Di


gamma  = .1
Lambda = 10
m = 1

J = [J_DL,J_SF,J_SO]
D = [D_DL,D_SF,D_SO]
labels = [f"$J_{{\\alpha,{title}}}$" for title in ('DL','SF','SO')]
w = np.linspace(-15,15,250)
'''
for i,Ji in enumerate(J):
    plt.figure()
    plt.plot(w,Ji(w))#,label=labels[i])

    plt.xlabel(r'$\omega$')
    plt.ylabel(r'$J_\alpha$')
    plt.xlim(0,max(w))
    #plt.legend()

    plt.savefig(f'report/figures/spectralD{i}.png')
'''
'''
part = ['Re','Im']
for i,Di in enumerate(D):
    fig,axs = plt.subplots(2,sharex=True,gridspec_kw={'hspace':0})
    D1 = Di(w,gamma)
    Re = D1.real
    Im = D1.imag
    axs[0].plot(w,Re,label='$\rm Re$')
    axs[1].plot(w,Im,label='$\rm Im$',c='orange')

    for j in range(2):
        axs[j].set_xlim(min(w),max(w))
        axs[j].set_xlabel(r'$\omega$')
        axs[j].set_ylabel(fr'$\rm {part[j]}$')
        #plt.xlim(0,max(w))
    #plt.legend()

    plt.savefig(f'report/figures/suscept-{i}.png')
plt.show()
'''
part = ['Re','Im']

labels = [f"$D_{{{title}}}$" for title in ('DL','SF','SO')]

fig,axs = plt.subplots(2,sharex=True,gridspec_kw={'hspace':0})
for i,Di in enumerate(D):
    D1 = Di(w,gamma)
    Re = D1.real
    Im = D1.imag
    axs[0].plot(w,Re,label=labels[i])
    axs[1].plot(w,Im,label=labels[i])

for j in range(2):
    axs[j].set_ylabel(fr'${{\rm {part[j]}}} [D_\alpha(\omega)] $')
    axs[j].axvline(Lambda,ls='dotted',color='r')
    axs[j].axvline(-Lambda,ls='dotted',color='r')

axs[0].legend()
axs[1].set_xlabel(r'$\omega$')
axs[1].set_xlim(min(w),max(w))
fig.tight_layout()
plt.savefig(f'figures/suscept-grouped.png')
plt.show()
